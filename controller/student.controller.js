function takeFormValue() {
  const code = document.querySelector("#txtMaSV").value;
  const name = document.querySelector("#txtTenSV").value;
  const email = document.querySelector("#txtEmail").value;
  const password = document.querySelector("#txtPass").value;
  const math = document.querySelector("#txtDiemToan").value * 1;
  const physics = document.querySelector("#txtDiemLy").value * 1;
  const chemistry = document.querySelector("#txtDiemHoa").value * 1;

  return { code, name, email, password, math, physics, chemistry };
}

function takeSearchValue() {
  return document.querySelector("#txtSearch").value.toLowerCase();
}

function createNewStudent(studentObj) {
  return new Student(studentObj);
}

function showAdjustStudentToForm(student) {
  document.querySelector("#txtMaSV").value = student.code;
  document.querySelector("#txtTenSV").value = student.name;
  document.querySelector("#txtEmail").value = student.email;
  document.querySelector("#txtPass").value = student.password;
  document.querySelector("#txtDiemToan").value = student.math;
  document.querySelector("#txtDiemLy").value = student.physics;
  document.querySelector("#txtDiemHoa").value = student.chemistry;
}

function lockElement(elementIdObj) {
  for (let element in elementIdObj) {
    document.getElementById(element).disabled = true;
  }
}

function unLockElement(elementIdObj) {
  for (let element in elementIdObj) {
    document.getElementById(element).disabled = false;
  }
}

function resetInput() {
  document.querySelector("#txtMaSV").value = "";
  document.querySelector("#txtTenSV").value = "";
  document.querySelector("#txtEmail").value = "";
  document.querySelector("#txtPass").value = "";
  document.querySelector("#txtDiemToan").value = "";
  document.querySelector("#txtDiemLy").value = "";
  document.querySelector("#txtDiemHoa").value = "";
}

function renderToHTML(studentList) {
  let htmlMarkup = ``;
  for (let index = 0; index < studentList.length; index++) {
    let currentStudent = studentList[index];

    htmlMarkup += `
    <tr>
      <td class="align-middle">${currentStudent.code}</td>
      <td class="align-middle">${currentStudent.name}</td>
      <td class="align-middle">${currentStudent.email}</td>
      <td class="align-middle">${currentStudent.average}</td>
      <td class="align-middle">
        <button onclick="removeStudent(${currentStudent.code})" class="btn btn-danger">Xóa</button>
        <button onclick="adjustStudent(${currentStudent.code})" class="btn btn-warning">Sửa</button>
      </td>
    </tr>
    `;
  }
  document.querySelector("#tbodySinhVien").innerHTML = htmlMarkup;
}
