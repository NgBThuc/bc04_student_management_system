const studentCodeInput = document.querySelector("#txtMaSV");
const studentNameInput = document.querySelector("#txtTenSV");
const studentEmailInput = document.querySelector("#txtEmail");
const studentPasswordInput = document.querySelector("#txtPass");
const studentMathInput = document.querySelector("#txtDiemToan");
const studentPhysicsInput = document.querySelector("#txtDiemLy");
const studentChemistryInput = document.querySelector("#txtDiemHoa");

function isRequired(value) {
  return value === "" ? false : true;
}

function isBetween(value, min, max) {
  return value < min || value > max ? false : true;
}

function isEmailValid(email) {
  const regex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
}

function isPasswordSecure(password) {
  const regex = new RegExp(
    "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
  );
  return regex.test(password);
}

function isDuplicateCode(studentList, value) {
  if (studentList.findIndex((student) => student.code === value) === -1) {
    return false;
  } else {
    return true;
  }
}

function showError(input, message) {
  const formGroup = input.parentElement;

  const error = formGroup.querySelector("span");
  error.textContent = message;
}

function removeError(input) {
  const formGroup = input.parentElement;
  const error = formGroup.querySelector("span");
  error.textContent = "";
}

function removeAllError() {
  let errorDisplayEl = document.querySelectorAll(".form-group > span");
  errorDisplayEl.forEach((element) => (element.textContent = ""));
}

function checkCode(studentList, value, isAdjust) {
  let valid = false;

  let min = 3;
  let max = 15;
  const studentCode = studentCodeInput.value.trim();

  if (isDuplicateCode(studentList, value) && !isAdjust) {
    showError(studentCodeInput, "Mã sinh viên không được trùng");
  } else if (!isRequired(studentCode)) {
    showError(studentCodeInput, "Mã sinh viên không được để trống");
  } else if (!isBetween(studentCode.length, min, max)) {
    showError(
      studentCodeInput,
      `Mã sinh viên phải bao gồm từ ${min} tới ${max} ký tự`
    );
  } else {
    removeError(studentCodeInput);
    valid = true;
  }

  return valid;
}

function checkName() {
  let valid = false;

  let min = 3;
  let max = 25;
  const studentName = studentNameInput.value.trim();

  if (!isRequired(studentName)) {
    showError(studentNameInput, "Tên sinh viên không được để trống");
  } else if (!isBetween(studentName.length, min, max)) {
    showError(
      studentNameInput,
      `Tên sinh viên phải bao gồm từ ${min} đến ${max} ký tự`
    );
  } else {
    removeError(studentNameInput);
    valid = true;
  }

  return valid;
}

function checkEmail() {
  let valid = false;

  const studentEmail = studentEmailInput.value.trim();

  if (!isRequired(studentEmail)) {
    showError(studentEmailInput, "Email sinh viên không được để trống");
  } else if (!isEmailValid(studentEmail)) {
    showError(studentEmailInput, "Vui lòng nhập email hợp lệ");
  } else {
    removeError(studentEmailInput);
    valid = true;
  }

  return valid;
}

function checkPassword() {
  let valid = false;

  const studentPassword = studentPasswordInput.value.trim();

  if (!isRequired(studentPassword)) {
    showError(studentPasswordInput, "Password không được để trống");
  } else if (!isPasswordSecure(studentPassword)) {
    showError(
      studentPasswordInput,
      "Mật khẩu phải bao gồm 8 ký tự trong đó có ít nhất 1 chữ cái thường, 1 chữ cái viết hoa, 1 số và 1 ký tự đặc biệt"
    );
  } else {
    removeError(studentPasswordInput);
    valid = true;
  }

  return valid;
}

function checkMath() {
  let valid = false;

  let min = 0;
  let max = 10;
  const studentMath = studentMathInput.value.trim();

  if (!isRequired(studentMath)) {
    showError(studentMathInput, "Điểm toán không được để trống");
  } else if (Number.isNaN(+studentMath)) {
    showError(studentMathInput, "Điểm toán phải là số");
  } else if (!isBetween(+studentMath, min, max)) {
    showError(
      studentMathInput,
      "Điểm toán chỉ được nằm trong khoảng từ 0 tới 10"
    );
  } else {
    removeError(studentMathInput);
    valid = true;
  }

  return valid;
}

function checkPhysics() {
  let valid = false;

  let min = 0;
  let max = 10;
  const studentPhysics = studentPhysicsInput.value.trim();

  if (!isRequired(studentPhysics)) {
    showError(studentPhysicsInput, "Điểm lý không được để trống");
  } else if (Number.isNaN(+studentPhysics)) {
    showError(studentPhysicsInput, "Điểm lý phải là số");
  } else if (!isBetween(+studentPhysics, min, max)) {
    showError(
      studentPhysicsInput,
      "Điểm lý chỉ được nằm trong khoảng từ 0 tới 10"
    );
  } else {
    removeError(studentPhysicsInput);
    valid = true;
  }

  return valid;
}

function checkChemistry() {
  let valid = false;

  let min = 0;
  let max = 10;
  const studentChemistry = studentChemistryInput.value.trim();

  if (!isRequired(studentChemistry)) {
    showError(studentChemistryInput, "Điểm hóa không được để trống");
  } else if (Number.isNaN(+studentChemistry)) {
    showError(studentChemistryInput, "Điểm hóa phải là số");
  } else if (!isBetween(+studentChemistry, min, max)) {
    showError(
      studentChemistryInput,
      "Điểm hóa chỉ được nằm trong khoảng từ 0 tới 10"
    );
  } else {
    removeError(studentChemistryInput);
    valid = true;
  }

  return valid;
}

function isValidate(studentList, value, isAdjust) {
  let isStudentCodeValid = checkCode(studentList, value, isAdjust);
  let isStudentNameValid = checkName();
  let isStudentEmailValid = checkEmail();
  let isStudentPasswordValid = checkPassword();
  let isMathValid = checkMath();
  let isPhysicsValid = checkPhysics();
  let isChemistryValid = checkChemistry();

  return (
    isStudentCodeValid &&
    isStudentNameValid &&
    isStudentEmailValid &&
    isStudentPasswordValid &&
    isMathValid &&
    isPhysicsValid &&
    isChemistryValid
  );
}
