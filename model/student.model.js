class Student {
  constructor(studentObj) {
    this.code = studentObj.code;
    this.name = studentObj.name;
    this.email = studentObj.email;
    this.password = studentObj.password;
    this.math = studentObj.math;
    this.physics = studentObj.physics;
    this.chemistry = studentObj.chemistry;
    this.average = (
      (studentObj.math + studentObj.physics + studentObj.chemistry) /
      3
    ).toFixed(2);
  }
}
