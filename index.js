let addStudentBtn = document.querySelector("#addStudentBtn");
let updateStudentBtn = document.querySelector("#updateStudentBtn");
let resetInputBtn = document.querySelector("#resetInputBtn");
let studentSearchBtn = document.querySelector("#studentSearchBtn");
let studentShowAllBtn = document.querySelector("#studentAllBtn");

let studentList = [];
let STUDENTLIST_STORAGE = "STUDENTLIST_STORAGE";

function init() {
  studentList = getStudentListFromLocalStorage();
  renderToHTML(studentList);
  lockElement({ updateStudentBtn });
}

function getStudentListFromLocalStorage() {
  if (!localStorage.getItem(STUDENTLIST_STORAGE)) {
    return [];
  }

  return JSON.parse(localStorage.getItem(STUDENTLIST_STORAGE));
}

function addStudent() {
  let student = takeFormValue();
  let newStudent = createNewStudent(student);
  studentList.push(newStudent);
}

function removeStudent(studentCode) {
  let removeIndex = studentList.findIndex(
    (student) => student.code == studentCode
  );

  if (removeIndex !== -1) {
    studentList.splice(removeIndex, 1);
  }

  saveToLocalStorage(studentList);
  renderToHTML(studentList);
}

function adjustStudent(studentCode) {
  let currentStudent = studentList.find(
    (student) => student.code == studentCode
  );
  lockElement({ txtMaSV, addStudentBtn, resetInputBtn });
  unLockElement({ updateStudentBtn });
  showAdjustStudentToForm(currentStudent);
}

function saveToLocalStorage(studentList) {
  localStorage.setItem(STUDENTLIST_STORAGE, JSON.stringify(studentList));
}

addStudentBtn.addEventListener("click", function () {
  if (!isValidate(studentList, studentCodeInput.value, false)) {
    return;
  }
  addStudent();
  saveToLocalStorage(studentList);
  renderToHTML(studentList);
  resetInput();
});

updateStudentBtn.addEventListener("click", function () {
  if (!isValidate(studentList, studentCodeInput.value, true)) {
    return;
  }

  let updateStudentCode = document.querySelector("#txtMaSV").value;
  let updateStudentIndex = studentList.findIndex(
    (student) => student.code == updateStudentCode
  );

  if (updateStudentIndex !== -1) {
    studentList[updateStudentIndex] = createNewStudent(takeFormValue());
  }
  saveToLocalStorage(studentList);
  renderToHTML(studentList);
  unLockElement({ txtMaSV, addStudentBtn, resetInputBtn });
  resetInput();
});

resetInputBtn.addEventListener("click", function () {
  resetInput();
  removeAllError();
});

studentSearchBtn.addEventListener("click", function () {
  let searchValue = takeSearchValue();
  let filteredStudentList = studentList.filter((student) =>
    student.name.toLowerCase().includes(searchValue)
  );

  renderToHTML(filteredStudentList);
});

studentShowAllBtn.addEventListener("click", function () {
  renderToHTML(studentList);
});

init();
